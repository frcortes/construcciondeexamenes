<?php
    class Examen extends Controller {

        function __construct() {
            parent::__construct();
        }

        function examen() {
            $this->view->render($this, 'examen');
        }

        function obtenerTemas() {
            $data = $this->model->obtenerTemas();
            if (is_array($data)) echo json_encode($data);
            else echo $data;
        }

        function obtenerCasosporTema() {
            $data = $this->model->obtenerCasosporTema($_POST['topId']);
            if (is_array($data)) echo json_encode($data);
            else echo $data;
        }

        function construirExamen() {
            echo $this->model->construirExamen($_POST['idTopics'], $_POST['nQuestions'], $_POST['testCode'], $_POST['testName'], $_POST['testDescription']);
        }

        function obtenerExamenes() {
            $data = $this->model->obtenerExamenes();
            if (is_array($data)) echo json_encode($data);
            else echo $data;
        }

        function obtenerCuerpoExamen() {
            $data = $this->model->obtenerCuerpoExamen($_POST['testId']);
            if (is_array($data)) echo json_encode($data);
            else echo $data;
        }
    }