<?php
    class Query {

        private $pdo;

        function __construct($user, $pass, $db) {
            try {
                $this->pdo = new PDO('mysql:host=localhost;dbname='.$db.';charset=utf8', $user, $pass, 
                    [
                        PDO::ATTR_EMULATE_PREPARES => false,
                        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
                    ]
                );
            } catch (PDOExcepcion $e) {
                print 'Error! '.$e->getMessage();
                die();
            }
        }

        function select($attr, $table, $where, $param) {
            try {
                if ($where == '') $query = "SELECT ".$attr." FROM ".$table;
                else $query = "SELECT ".$attr." FROM ".$table." WHERE ".$where;

                $sth = $this->pdo->prepare($query);

                $sth->execute($param);
                
                $response = $sth->fetchAll(PDO::FETCH_ASSOC);
                
                return array('results' => $response);
            } catch (PDOException $e) {
                return $e->getMessage();
            }
            $pdo = null;
        }

        function insert($table, $value, $param) {
            try {
                $query = "INSERT INTO ".$table.$value;
                $sth = $this->pdo->prepare($query);
                $sth->execute($param);
                return true;
            } catch (PDOException $e) {
                return $e->getMessage();
            }
            $pdo = null;
        }

        function update($table, $field, $value, $where, $param) {
            try {
                $query = "UPDATE ".$table." SET ".$field." = ".$value." WHERE ".$where;
                $sth = $this->pdo->prepare($query);

                if ($sth->execute($param)) return 1;
                else return $sth;
                
            } catch (PDOExcepcion $e) {
                return $e->getMessage();
            }
            $pdo = null;
        }

    }