<?php
    class Controller {

        function __construct() {
            $this->view = new View();
            $this->loadClassModels();
        }

        function loadClassModels() {
            $model = get_class($this).'Model';
            $path = 'Models/'.$model.'.php';
            if (file_exists($path)) {
                require $path;
                $this->model = new $model();
            }
        }
    }