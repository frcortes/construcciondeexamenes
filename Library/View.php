<?php
    class View {
        
        function render($controller, $view) {
            $nameController = get_class($controller);

            require VIEWS.DFT.'head.html';
            require VIEWS.$nameController.'/'.$view.'.html';
            require VIEWS.DFT.'footer.html';
        }
    }