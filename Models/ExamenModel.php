<?php
    class ExamenModel extends Connection {
        public function __construct() {
            parent::__construct();
        }

        public function obtenerTemas() {
            $attr = 't.tem_codigo as "tem_codigo", t.tem_nombre as "tem_nombre", t.tem_descripcion as "tem_descripcion", count(*) as npreguntas';
            $table = 'tema t, caso c, pregunta p';
            $where = 't.tem_codigo = c.tem_codigo and c.cas_codigo = p.cas_codigo GROUP by t.tem_codigo';
            return $this->db->select($attr, $table, $where, null);
        }

        function obtenerCasosporTema($topId) {
            $attr = 'distinct t.tem_nombre as "tem_nombre", c.cas_nombre as "cas_nombre", c.cas_descripcion as "cas_descripcion", count(*) as "npreguntas"';
            $table = 'tema t, caso c, pregunta p';
            $where = 't.tem_codigo = c.tem_codigo and c.cas_codigo = p.cas_codigo and t.tem_codigo = :topId group by c.cas_codigo';
            $param = array('topId' => $topId);
            return $this->db->select($attr, $table, $where, $param);
        }

        function construirExamen($idTopics, $nQuestions, $testCode, $testName, $testDescription) {
            $idTopics = explode(',', $idTopics);
            $nQuestions = explode(',', $nQuestions);
            $where = 'pru_codigo = :testCode or pru_nombre = :testName';
            $param = array(
                'testCode' => $testCode,
                'testName' => $testName
            );

            $resp = $this->db->select('pru_codigo', 'prueba', $where, $param);
            if (is_array($resp)) {
                $resp = $resp['results'];

                if (count($resp) == 0) {
                    $value = '(pru_codigo, pru_nombre, pru_descripcion) VALUES (:testCode, :testName, :testDescription)';
                    $param = array(
                        'testCode' => $testCode,
                        'testName' => $testName,
                        'testDescription' => $testDescription
                    );
                    $res = $this->db->insert('prueba', $value, $param);

                    if ($res == 1) {
                        $values = '';
                        for ($i = 0; $i < count($idTopics); $i++) {
                            if ($i == 0) $values .= "(pru_codigo, tem_codigo, pt_num_preg) VALUES ($testCode, $idTopics[0], $nQuestions[0])";
                            else $values .= ", ($testCode, $idTopics[$i], $nQuestions[$i])";
                        }
                        $data = $this->db->insert('prueba_tema', $values, null);
                        return $data;
                    } else return $res;
                } else return -1;
            } else return $resp;
        }

        function obtenerExamenes() {
            return $this->db->select('*', 'prueba', null, null);
        }
        
        function obtenerCuerpoExamen($testId) {
            $where = '';
            $param = '';
            return $this->db->select('', '', $where, $param);
        }

    }