 create table prueba(
	pru_codigo bigint AUTO_INCREMENT PRIMARY KEY,
    pru_nombre varchar (50) not null,
    pru_descripcion varchar(365)
);

create table tema(
	tem_codigo bigint PRIMARY KEY,
    tem_nombre varchar (50) not null,
    tem_descripcion varchar(365)
);

create table prueba_tema(
	pru_codigo bigint,
    tem_codigo bigint,
    pt_descripcion varchar(365),
    pt_num_preg bigint not null,
    FOREIGN Key (pru_codigo) REFERENCES prueba(pru_codigo),
    FOREIGN KEY (tem_codigo) REFERENCES tema(tem_codigo)
);

CREATE TABLE caso (
	cas_codigo bigint PRIMARY KEY,
    tem_codigo bigint,
    cas_nombre varchar(50) not null,
    cas_descripcion varchar(365),
    FOREIGN KEY (tem_codigo) REFERENCES tema(tem_codigo)
);

create table pregunta (
	pre_codigo bigint primary key,
    cas_codigo bigint,
    pre_pregunta varchar(365) not null,
    pre_descripcion varchar(365),
    foreign key (cas_codigo) REFERENCES caso(cas_codigo)
);

create table respuesta(
	res_codigo bigint PRIMARY KEY,
    pre_codigo bigint,
    res_respuesta varchar(365) not null,
    res_estado varchar(1) DEFAULT '0',
    FOREIGN key (pre_codigo) REFERENCES pregunta(pre_codigo)
);


-- Poblado


INSERT INTO `tema`(`tem_codigo`, `tem_nombre`, `tem_descripcion`) VALUES (1,'Ofimática avanzada','Es necesario'),
(2,'Razonamiento Cuantitativo','Es necesario'),
(3,'Inglés Básico','Es necesario'),
(4,'Inglés Avanzado','Es necesario'),
(5,'Expresión Corporal','Es necesario');



INSERT INTO `caso` (`cas_codigo`,`tem_codigo`,`cas_nombre`,`cas_descripcion`) VALUES (1,2,"Magna Tellus Corp.","tellus eu augue porttitor interdum."),(2,3,"Diam Proin Dolor Industries","penatibus et magnis"),(3,2,"Eleifend Institute","Morbi metus."),(4,5,"Hendrerit A Limited","velit dui, semper et, lacinia vitae, sodales at,"),(5,3,"Nullam Lobortis Corporation","convallis dolor. Quisque tincidunt pede ac urna. Ut tincidunt"),(6,3,"Velit Institute","mattis ornare, lectus ante dictum mi, ac mattis velit"),(7,1,"Ipsum PC","massa. Suspendisse eleifend. Cras sed"),(8,2,"Enim Suspendisse PC","tempus mauris erat"),(9,1,"Lorem Corporation","Cras vulputate velit eu sem. Pellentesque ut ipsum"),(10,1,"Sed Sem Egestas Incorporated","cursus in, hendrerit consectetuer,"),(11,5,"Semper Erat In Industries","per conubia nostra,"),(12,5,"Neque Sed Eget Corp.","lectus pede et risus. Quisque libero lacus, varius et,"),(13,3,"Lectus Industries","et ultrices posuere cubilia Curae; Phasellus ornare."),(14,1,"Quis Diam Luctus Consulting","orci, adipiscing non, luctus sit amet, faucibus ut, nulla. Cras"),(15,1,"Mattis Cras Associates","nibh. Phasellus nulla. Integer vulputate,");
INSERT INTO `caso` (`cas_codigo`,`tem_codigo`,`cas_nombre`,`cas_descripcion`) VALUES (16,1,"Et Ipsum Cursus Consulting","malesuada fames ac turpis egestas. Fusce aliquet magna a"),(17,5,"Arcu Sed Et Corporation","imperdiet, erat nonummy ultricies ornare,"),(18,2,"Curabitur Vel Lectus Incorporated","Donec vitae erat vel pede blandit congue. In scelerisque scelerisque"),(19,4,"Arcu Incorporated","lobortis quam a felis ullamcorper viverra. Maecenas"),(20,2,"Pharetra Associates","lorem ipsum sodales purus, in molestie"),(21,1,"Montes Nascetur Ridiculus Consulting","dolor vitae dolor. Donec fringilla. Donec feugiat"),(22,2,"Mauris Ipsum Ltd","ultrices posuere"),(23,2,"Vitae Purus Incorporated","arcu. Vestibulum ante ipsum primis in faucibus orci luctus et"),(24,2,"At Risus Nunc Corp.","dis parturient montes, nascetur ridiculus mus. Proin"),(25,2,"Quam Industries","magnis dis parturient montes,"),(26,1,"Est Congue A Incorporated","vel"),(27,2,"Rutrum Magna Institute","cursus in, hendrerit consectetuer,"),(28,5,"Metus Vivamus Euismod Industries","sociis natoque penatibus et"),(29,2,"Id Ante Nunc LLP","sociis natoque penatibus"),(30,4,"Lacus PC","arcu iaculis enim, sit amet");


INSERT INTO `pregunta` (`pre_codigo`,`cas_codigo`,`pre_pregunta`,`pre_descripcion`) VALUES (1,26,"Vel LLC","sagittis augue, eu tempor"),(2,14,"Ultricies Ligula Foundation","euismod est arcu ac orci."),(3,1,"Vel Incorporated","vitae mauris sit"),(4,7,"Eleifend Nunc Ltd","eleifend. Cras sed leo. Cras vehicula"),(5,24,"Ultricies Adipiscing Enim Foundation","Morbi quis urna. Nunc quis arcu vel quam"),(6,16,"Semper Auctor Mauris Company","felis ullamcorper viverra. Maecenas iaculis aliquet diam. Sed"),(7,1,"Nec Quam Corp.","et ultrices"),(8,12,"Sed Neque PC","venenatis"),(9,9,"Etiam Laoreet Libero Foundation","interdum. Curabitur dictum. Phasellus in felis. Nulla tempor augue"),(10,10,"Libero Industries","Fusce dolor quam,"),(11,9,"Molestie Dapibus Ligula PC","mauris blandit"),(12,20,"A Dui Cras Ltd","Nam interdum enim non"),(13,21,"Metus Corp.","Quisque fringilla euismod enim. Etiam"),(14,4,"Sit Amet Risus LLP","Suspendisse sagittis. Nullam vitae diam."),(15,29,"Turpis Nulla Aliquet LLP","lectus. Cum sociis");
INSERT INTO `pregunta` (`pre_codigo`,`cas_codigo`,`pre_pregunta`,`pre_descripcion`) VALUES (16,22,"Convallis In Cursus Foundation","Praesent eu dui. Cum sociis natoque penatibus et magnis dis"),(17,11,"Purus Limited","Vestibulum ante"),(18,9,"Blandit Foundation","Cras eget"),(19,13,"Ornare Libero Corporation","risus. Quisque libero lacus, varius et, euismod"),(20,14,"Fusce Aliquet Corp.","est, mollis non, cursus non, egestas a,"),(21,11,"Sit Amet Consectetuer PC","dictum ultricies ligula. Nullam enim. Sed nulla ante, iaculis nec,"),(22,20,"Viverra Corp.","urna. Nullam lobortis quam a felis ullamcorper"),(23,7,"Egestas Sed Institute","Donec est."),(24,4,"Semper Rutrum Limited","erat vitae"),(25,16,"Eget Lacus Mauris Industries","a nunc. In at pede. Cras"),(26,25,"Ac Nulla Limited","diam. Duis mi enim, condimentum"),(27,24,"Risus LLP","eu augue porttitor interdum. Sed auctor odio a purus."),(28,19,"Est Tempor Incorporated","fringilla, porttitor vulputate,"),(29,3,"Donec Incorporated","accumsan convallis, ante lectus convallis est,"),(30,15,"Sed Tortor LLP","senectus et netus et malesuada fames ac turpis egestas.");
INSERT INTO `pregunta` (`pre_codigo`,`cas_codigo`,`pre_pregunta`,`pre_descripcion`) VALUES (31,17,"Quisque Imperdiet Erat LLC","natoque"),(32,1,"Et Institute","ullamcorper viverra. Maecenas iaculis aliquet diam. Sed diam lorem,"),(33,2,"Non Magna Incorporated","Aliquam erat volutpat. Nulla facilisis. Suspendisse"),(34,16,"Libero Industries","Aenean sed pede nec"),(35,1,"Metus Vitae Corporation","ornare, elit elit fermentum risus, at"),(36,23,"Lobortis Nisi Nibh Limited","tincidunt, neque vitae semper egestas, urna justo faucibus lectus, a"),(37,21,"Euismod Et Commodo LLP","molestie sodales. Mauris"),(38,28,"Dignissim Magna A Institute","Proin eget odio. Aliquam vulputate ullamcorper magna."),(39,3,"Nisl Maecenas Malesuada LLP","commodo ipsum. Suspendisse non leo."),(40,18,"Nulla Consulting","lorem, eget mollis lectus pede"),(41,12,"Neque Sed Sem Company","Vivamus rhoncus. Donec est. Nunc ullamcorper, velit in"),(42,21,"Dictum Eu Eleifend PC","quis, pede. Suspendisse dui. Fusce diam"),(43,12,"Fusce Fermentum Fermentum LLP","diam"),(44,9,"Donec Luctus Aliquet Consulting","Nulla tempor"),(45,16,"Nam Corporation","Phasellus fermentum convallis ligula. Donec luctus aliquet");
INSERT INTO `pregunta` (`pre_codigo`,`cas_codigo`,`pre_pregunta`,`pre_descripcion`) VALUES (46,13,"Iaculis Lacus LLC","et netus et malesuada fames ac turpis egestas."),(47,6,"Turpis Aliquam Adipiscing Inc.","sit amet metus. Aliquam erat volutpat."),(48,26,"Egestas Duis Ac PC","magna et"),(49,22,"Donec Corp.","magna nec quam. Curabitur vel lectus. Cum sociis natoque"),(50,10,"Donec Corporation","sem eget massa. Suspendisse eleifend."),(51,10,"Sodales PC","Aliquam gravida mauris ut"),(52,19,"Hymenaeos Corp.","ornare tortor"),(53,5,"Magna Ut LLP","malesuada id, erat. Etiam vestibulum massa rutrum magna."),(54,21,"Fringilla Donec Feugiat Consulting","magna tellus faucibus leo,"),(55,10,"Ipsum Suspendisse Inc.","lacus, varius et, euismod et, commodo at, libero."),(56,21,"Neque Sed Company","Aenean massa."),(57,3,"Feugiat Lorem Ipsum Inc.","feugiat nec,"),(58,3,"Volutpat Nulla Ltd","odio,"),(59,28,"Tincidunt Associates","vel quam"),(60,2,"Blandit Enim Consequat Incorporated","dictum. Phasellus");
INSERT INTO `pregunta` (`pre_codigo`,`cas_codigo`,`pre_pregunta`,`pre_descripcion`) VALUES (61,11,"Interdum Feugiat Limited","dolor. Fusce feugiat. Lorem ipsum dolor sit amet, consectetuer adipiscing"),(62,19,"Arcu Vestibulum Limited","cursus luctus, ipsum leo elementum sem, vitae aliquam eros"),(63,9,"Mauris Vel Turpis Industries","ipsum"),(64,16,"Cursus Associates","magna sed dui. Fusce aliquam, enim"),(65,11,"Eget Magna Suspendisse Foundation","faucibus id, libero. Donec"),(66,16,"Sed Foundation","turpis nec mauris blandit mattis."),(67,27,"Erat Nonummy Company","malesuada. Integer id magna et ipsum cursus vestibulum. Mauris"),(68,15,"Ipsum LLP","vitae"),(69,22,"Eu LLP","semper tellus id"),(70,11,"Integer Eu Lacus Corp.","arcu ac orci. Ut semper pretium"),(71,16,"Non Enim Mauris LLC","Ut nec urna et arcu imperdiet ullamcorper. Duis at"),(72,21,"Nec Company","sagittis felis. Donec tempor, est ac mattis semper, dui lectus"),(73,12,"Gravida Molestie Arcu Ltd","eleifend non, dapibus rutrum, justo. Praesent"),(74,6,"Molestie Pharetra Institute","sit amet, consectetuer adipiscing"),(75,24,"Egestas Nunc Sed Institute","auctor ullamcorper, nisl arcu iaculis enim, sit");
INSERT INTO `pregunta` (`pre_codigo`,`cas_codigo`,`pre_pregunta`,`pre_descripcion`) VALUES (76,1,"Dictum Ultricies Incorporated","eu, accumsan sed, facilisis vitae,"),(77,4,"Ultricies Dignissim Corp.","nec urna et"),(78,16,"Torquent Per Conubia Associates","ac sem"),(79,10,"Arcu Vestibulum Incorporated","erat. Etiam vestibulum massa rutrum"),(80,22,"Sed Facilisis Vitae Institute","quam vel sapien imperdiet ornare. In"),(81,15,"At Risus Nunc LLC","quis turpis vitae purus gravida sagittis. Duis gravida."),(82,26,"Felis Industries","sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus"),(83,1,"Donec Consulting","odio. Nam interdum enim non nisi. Aenean"),(84,28,"Nullam Vitae LLC","luctus. Curabitur egestas"),(85,8,"Nulla Interdum Curabitur Consulting","ipsum. Donec"),(86,10,"Ante Blandit Viverra Consulting","pulvinar arcu et pede. Nunc"),(87,3,"Sem Ut Inc.","vitae"),(88,24,"At Velit Limited","fringilla cursus purus."),(89,6,"Imperdiet Ullamcorper Incorporated","enim. Mauris quis turpis vitae purus gravida sagittis. Duis"),(90,27,"Eleifend Non Ltd","dolor dolor, tempus non, lacinia");
INSERT INTO `pregunta` (`pre_codigo`,`cas_codigo`,`pre_pregunta`,`pre_descripcion`) VALUES (91,28,"Nunc Ltd","sed, sapien. Nunc"),(92,26,"Rutrum Ltd","ligula. Aenean euismod mauris eu elit. Nulla"),(93,28,"Commodo Hendrerit Donec Company","primis in"),(94,13,"Natoque Corporation","sem molestie sodales. Mauris"),(95,5,"Neque Industries","quam vel sapien imperdiet ornare. In faucibus. Morbi"),(96,27,"Et Industries","cursus,"),(97,11,"Eros Turpis Non PC","ullamcorper eu, euismod ac, fermentum"),(98,18,"Odio Limited","luctus et ultrices posuere cubilia Curae; Donec"),(99,11,"Malesuada Corp.","eros."),(100,2,"Id Risus Quis Corporation","Donec felis orci, adipiscing");

-- Consultas

-- Temas con cantidad de preguntas
select t.tem_codigo as "tem_codigo", t.tem_nombre as "tem_nombre", t.tem_descripcion as "tem_descripcion", count(*) as npreguntas from tema t, caso c, pregunta p where t.tem_codigo = c.tem_codigo and c.cas_codigo = p.cas_codigo GROUP by t.tem_codigo