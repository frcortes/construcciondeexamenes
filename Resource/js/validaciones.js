var validarNombre = (name, min, max) => {
    if (name.value == '') {
        toastr.warning('Debes ingresar el nombre del examen', 'Tener en cuenta')
        name.focus()
        return false
    } else if (name.value.length < min) {
        toastr.warning('El nombre del examen debe contener por lo menos ' + min + ' caracteres', 'Tener en cuenta')
        name.focus()
        return false
    } else if (name.value.length > max) {
        toastr.warning('El nombre del examen debe contener como máximo ' + max + ' caracteres', 'Tener en cuenta')
        name.focus()
        return false
    } else if (!/^([A-Za-zÁÉÍÓÚñáéíóúÑ]{0}?[A-Za-zÁÉÍÓÚñáéíóúÑ\'][\s]*)+([A-Za-zÁÉÍÓÚñáéíóúÑ]{0}?[A-Za-zÁÉÍÓÚñáéíóúÑ\'])[\s]*?([A-Za-zÁÉÍÓÚñáéíóúÑ]{0}?[A-Za-zÁÉÍÓÚñáéíóúÑ\'])?$/g.test(name.value)) {
        toastr.warning('El nombre del examen solo debe contener letras', 'Tener en cuenta')
        name.focus()
        return false
    }
    return true
}