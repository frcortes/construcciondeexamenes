//configuraciones
    toastr.options.preventDuplicates = true

$().ready(() => {
    let urlActual = window.location.pathname
    if (urlActual == PATHNAME || urlActual == PATHNAME + 'Examen/examen') {
        fillTbodyTopics()
        obtenerExamenes()
    }

//--------------------------------codigo para dar responsive a las tablas-------------------------------------------
    // inspired by http://jsfiddle.net/arunpjohny/564Lxosz/1/
    $('.table-responsive-stack').find("th").each(function (i) {
        $('.table-responsive-stack td:nth-child(' + (i + 1) + ')').prepend('<span class="table-responsive-stack-thead">'+ $(this).text() + ':</span> ');
        $('.table-responsive-stack-thead').hide();
    });

    $( '.table-responsive-stack' ).each(function() {
        var thCount = $(this).find("th").length;
        var rowGrow = 100 / thCount + '%';
        //console.log(rowGrow);
        $(this).find("th, td").css('flex-basis', rowGrow);
    });
        
    function flexTable(){
        if ($(window).width() < 768) {
            $(".table-responsive-stack").each(function (i) {
                $(this).find(".table-responsive-stack-thead").show();
                $(this).find('thead').hide();
            });    
        // window is less than 768px   
        } else {
            $(".table-responsive-stack").each(function (i) {
                $(this).find(".table-responsive-stack-thead").hide();
                $(this).find('thead').show();
            });    
        }
        // flextable   
    }      
    
    flexTable();
    
    window.onresize = function(event) {
        flexTable();
    };
//--------------------------------FIN de codigo para dar responsive a las tablas---------------------------------------
})

var fillTbodyTopics = () => {
    $.ajax({
        url: URL + 'Examen/obtenerTemas',
        data: {},
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST'
    }).done( res => {
        //console.log(res)
        try {
            let item = JSON.parse(res)
            localStorage.setItem('topics', res)
            let count = 1
            let tr = ''
            item.results.forEach(ele => {
                tr += '<tr>'
                tr += '<td><input type="checkbox" id="cbitem' + count + '" value="'+ ele.tem_codigo +'"/></td>'
                tr += '<td>' + ele.tem_nombre + '</td><td>' + ele.tem_descripcion + '</td><td>' + ele.npreguntas + '</td>'
                tr += '<td><input id="npreg' + count + '" type="number" class="form-control"></td>'
                tr += '<td><button onclick="cargarCasos(' + ele.tem_codigo + ')" class="btn btn-info btn-block" data-toggle="modal" data-target=".bd-example-modal-lg">Ver</button></td>'
                tr += '</tr>'
                count++
            });
            $('#tbodytest > tr').remove()
            $('#tbodytest').append(tr)
        } catch (error) {
            toastr.error(error, 'Algo ha salido mal')
        }
    })
}

var cargarCasos = (topId) => {
    let tittletopic = document.getElementById('tittletopic')
    let data = new FormData()
    data.append('topId', topId)
    $.ajax({
        url: URL + 'Examen/obtenerCasosporTema',
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST'
    }).done( res => {
        //console.log(res)
        try {
            let item = JSON.parse(res)
            tittletopic.innerText = item.results[0].tem_nombre
            let tr = ''
            item.results.forEach(ele => {
                tr += '<tr>'
                tr += '<td>' + ele.cas_nombre + '</td><td>' + ele.cas_descripcion + '</td>'
                tr += '<td>' + ele.npreguntas + '</td><td><button class="btn btn-block btn-warning">Modificar</button></td>'
                tr += '<td><button class="btn btn-block btn-danger">Eliminar</button></td>'
                tr += '</tr>'
            })

            $('#tbodyCases > tr').remove()
            $('#tbodyCases').append(tr)
        } catch (error) {
            toastr.error(error, 'Algo ha salido mal')
        }
    })
}

var registerTest = () => {
    var testCode = document.getElementById('testCode')
    var testName = document.getElementById('testName')
    var testDescription = document.getElementById('testDescription')
    if (validarNombre(testName, 5, 50) && validarNombre(testDescription, 5, 365) && confirm('¿Seguro deseas construir ésta prueba?')) {
        var idTopics = []
        var nQuestions = []
        try {
            let item = JSON.parse(localStorage.getItem('topics'))
            for (let i = 1; i <= item.results.length; i++) {
                let check = document.getElementById('cbitem' + i)
                let input = document.getElementById('npreg' + i)
                if (check.checked) {
                    idTopics.push(check.value)
                    if (input.value != '') nQuestions.push(input.value)
                }
            }
            if (idTopics.length != nQuestions.length || idTopics.length == 0 || nQuestions.length == 0) throw '1'
            var data = new FormData()
            data.append('idTopics', idTopics)
            data.append('nQuestions', nQuestions)
            data.append('testCode', testCode.value)
            data.append('testName', testName.value)
            data.append('testDescription', testDescription.value)
            $.ajax({
                url: URL + 'Examen/construirExamen',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST'
            }).done( res => {
                //console.log(res)
                if (res == 1) {
                    toastr.success('Examen Construido', 'Proceso Exitoso')
                    document.getElementById('formBuild').reset()
                    obtenerExamenes()
                } else if (res == -1) toastr.warning('Esta prueba ya ha sido creada', 'Ten en Cuenta')
                else toastr.error(res, 'Algo ha salido mal')
            })
        } catch (error) {
            if (error == 1) toastr.warning('Selecciona un tema o Ingrese número de preguntas', 'Ten en cuenta')
            else toastr.error(error, 'Algo ha salido mal')
        }
    }
}

var obtenerExamenes = () => {
    $.ajax({
        url: URL + 'Examen/obtenerExamenes',
        data: {},
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST'
    }).done( res => {
        //console.log(res)
        try {
            let item = JSON.parse(res)
            let tr = ''
            item.results.forEach(ele => {
                tr += '<tr>'
                tr += '<td>' + ele.pru_nombre + '</td><td>' + ele.pru_descripcion + '</td>'
                tr += '<td><button onclick="obtenerCuerpoExamen(' + ele.pru_codigo + ')" class="btn btn-block btn-info" data-toggle="modal" data-target=".bd-example-modal-lg2">Ver</button></td>'
                tr += '</tr>'
            })

            $('#tbodyTests > tr').remove()
            $('#tbodyTests').append(tr)
        } catch (error) {
            toastr.error(res, 'Algo ha salido mal')
        }
    })
}

var obtenerCuerpoExamen = testId => {
    var data = new FormData()
    data.append('testId', testId)
    $.ajax({
        url: URL + 'Examen/obtenerCuerpoExamen',
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST'
    }).done( res => {
        console.log(res)
        
    })
}